﻿namespace WebApplication2.Controllers
{
    public class BMIData
    {
        public int Height { get; internal set; }
        public float Weight { get; internal set; }
        public float BMI { get; internal set; }
        public string Level { get; internal set; }
    }
}