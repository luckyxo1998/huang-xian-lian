﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    [MetadataType(typeof(TableMetadata))]
    public partial class Table
    {
    }

    public class TableMetadata
    {
        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("帳號")]
        [StringLength(10)]
        public int Id { get; set; }
        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("姓名")]
        [StringLength(10)]
        public string Name { get; set; }
        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("Email")]
        [EmailAddress]
        public string Email { get; set; }
        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("密碼")]
        [StringLength(10)]
        public string Password { get; set; }
        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("森日")]
        [StringLength(10)]
        public Nullable<System.DateTime> Birthday { get; set; }
        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("性別")]
        
        public Nullable<bool> Gender { get; set; }
    }
}